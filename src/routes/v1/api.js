const eventValidation = require("../../validations/eventValidations");
//https://dev.to/nedsoft/a-clean-approach-to-using-express-validator-8go
const express = require('express');
const router = new express.Router();
const isAuth = require('../../middleware/auth');
const isAuthUpdate = require('../../middleware/authUpdate');
const upload = require('../../middleware/multer');
const usersControllers = require('../../controllers/api/v1/clients/users');
const eventControllers = require('../../controllers/api/v1/clients/events');
const settingsController = require('../../controllers/api/v1/clients/settings');

const offerControllers = require('../../controllers/api/v1/clients/attendees');
const paymentControllers = require('../../controllers/api/v1/clients/payment');
const transactionControllers = require('../../controllers/api/v1/clients/transactions');
//get all users
router.get('/users', usersControllers.index);
router.post('/users', usersControllers.store);
router.get('/users/me', usersControllers.show);
router.patch('/users/:id', usersControllers.update);
router.delete('/users/:id', isAuth, usersControllers.destroy);

router.get('/users/me:id/:token', isAuth, usersControllers.getUserMe);
router.get('/poster/:id', usersControllers.getPosterRating);
router.get('/tasker/:id', usersControllers.getTaskerRating);
router.post('/reviewer', isAuthUpdate, usersControllers.findReviewID);
router.delete('/users/me', isAuth, usersControllers.deleteUserMe);
router.patch('/users/me', isAuth, usersControllers.updateUserMe);

// router.post('/facebook', usersControllers.facebookSignIn);
// router.post('/google', usersControllers.googleSignIn);
router.post('/users/logoutAll', isAuth, usersControllers.logoutAll);
router.post('/users/logout', isAuth, usersControllers.logout);
router.post('/users/signIn', usersControllers.signIn);
router.post('/users/signUp', usersControllers.signUp);
router.get("/users/me/get:email", isAuth, usersControllers.getUserDefault);
router.delete("/users", usersControllers.deleteUserMe);
router.put("/users/updateOne", isAuth, usersControllers.updateUserInformation);
router.post("/users/forget:email", usersControllers.sendForgetEmail);
router.post('/events', eventValidation.post, isAuth, eventControllers.store);
router.get('/events', eventValidation.get, eventControllers.index);
router.get('/events/:slug', eventValidation.show, isAuth, eventControllers.show);
router.put('/events/:id', eventValidation.update, isAuth, eventControllers.update);
router.delete('/events/:id', eventValidation.del, isAuth, eventControllers.delete);
router.post('/events/addAttachment/:id', isAuth, eventControllers.addAttachment);
//setting for react

router.get('/events/me/get:id', eventControllers.indexMe);

router.get('/settings', settingsController.index);

router.post('/attendees', isAuth, offerControllers.store);
router.put('/attendees/:id', isAuth, offerControllers.update);
router.delete('/attendees/:id', isAuth, offerControllers.delete);

router.get('/search', eventControllers.search);

router.post('/payments', isAuth, paymentControllers.store);

router.post('/transactions', isAuth, transactionControllers.store);
router.get('/transactions/me', isAuth, transactionControllers.showMe);

router.post('/upload', upload.array('photos'), (req, res) => {
    res.status(200).json(req.files);
});

router.get('/', async (req, res) => {
    return res.status(200).send("Failed");
});


module.exports = router;
