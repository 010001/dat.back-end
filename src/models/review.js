const mongosse = require('mongoose');

const reviewSchema = new mongosse.Schema(
    {
        poster_id: {
            type: mongosse.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        tasker_id:{
            type: mongosse.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        rate: {
            type: Number,
            required: true,
            trim: true,
        },
        comments: {
            type: String,
            required: true,
            trim: true,
        },
    }, {timestamps: true},
);


const Review = mongosse.model('Review', reviewSchema);
module.exports = Review;

