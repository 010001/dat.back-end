const mongosse = require('mongoose');

const transactionSchema = new mongosse.Schema(
    {
        payer_id: {
            type: mongosse.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        payee_id: {
            type: mongosse.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        amount: {
            type: Number,
            required: true,
            trim: true,
        },
        description: {
            type: String,
        },
        currency: {
            type: String,
            required: true,
        },
    }, {timestamps: true}
);


const Transaction = mongosse.model('Transaction', transactionSchema);
module.exports = Transaction;
