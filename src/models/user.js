const mongosse = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const userSchema = new mongosse.Schema(
    {
        name: {
            firstName: {type: String, default: "", trim: true},
            lastName: {type: String, default: "", trim: true},
        },
        email: {
            type: String,
            unique: true,
            required: true,
            trim: true,
            index: true,
            validate(value) {
                if (!validator.isEmail(value)) {
                    throw new Error('Email is invalid');
                }
            }
        },
        password: {
            type: String,
            required: true,
            minlength: 7,
            trim: true,
            validate(value) {
                if (value.toLowerCase().includes('password')) {
                    throw new Error('Password cannot contain password');
                }
            }
        },
        age: {
            type: String,
            default: ""
        },
        address: {
            suburb: {type: String, default: ""},
            state: {type: String, default: ""},
            country: {type: String, default: ""}
        },
        images: {
            type: String,
            default: ""
        },
        about: {
            tagLine: {type: String, default: ""},
            description: {type: String, default: ""},
        },
        rating: {
            type: Number,
            default: 0
        },
        totalPeople: {
            type: Number,
            default: 0
        },
        totalRating: {
            type: Number,
            default: 0
        },
        skills: [{
            education: {type: String, default: ""},
            specialities: {type: String, default: ""},
            languages: {type: String, default: ""},
            work: {type: String, default: ""},
            transportation: [{type: String, default: ""}],
        }],
        portfolio: [{
            type: String, default: ""
        }],
        badges: [{
            PoliceCheck: {type: Boolean, default: false},
            PaymentMethod: {
                BillingAddress: {type: String, default: ""},
                PostCode: {type: Number, default: 0},
                BankAccount: {
                    AccountName: {type: String, default: ""},
                    AccountNumber: {type: String, default: ""}
                }
            }
        }],
        phoneNumber: {type: String, default: ""},
        tokens: [{
            token: {
                type: String,
                required: true,
            }
        }],
    }, {timestamps: true},
);

userSchema.methods.toJSON = function () {

    const user = this;
    const userObject = user.toObject();

    delete userObject.password;
    delete userObject.tokens;

    return userObject;
};

userSchema.methods.generateAuthToken = async function () {
    console.log("abc");
    const user = this;
    const token = jwt.sign({_id: user._id.toString()}, 'thisismynewcourse');
    // try {
    //     const decoded = jwt.verify(token, 'thisismynewcourse');
    //     console.log(token);
    // }
    // catch (e) {
    //     console.log("fxxk: ", e);
    // }
    user.tokens = user.tokens.concat({token});
    await user.save();
    return token;
};

userSchema.methods.caluateRating = async function (rate) {
    const user = this;
    user.totalRating += parseInt(rate);
    user.totalPeople += 1;
    user.rating = user.totalRating / user.totalPeople;

    return user;
};

userSchema.methods.addReview = async function (review, type) {
    const user = this;
    switch (type) {
        case ReviewTypeEnum.tasker:
            user.tasker.push(review);
            break;
        case ReviewTypeEnum.poster:
            user.poster.push(review);
    }
    return user;
};


userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({email});
    if (!user) {
        throw new Error("401");
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
        throw new Error("401");
    }
    return user;
};
//https://mongoosejs.com/docs/guide.html#virtuals
userSchema.virtual('fullName').get(function () {
    return this.name.firstName + ' ' + this.name.lastName;
});


//Hash the plain text password before saving
userSchema.pre('save', async function (next) {
    const user = this;

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8);
    }
    next();
});

const ReviewTypeEnum = {"tasker": "tasker", "poster": "poster"};
Object.freeze(ReviewTypeEnum);


const User = mongosse.model('User', userSchema);

module.exports = ReviewTypeEnum;
module.exports = User;
