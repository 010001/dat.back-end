const mongosse = require('mongoose');

const attendeeSchema = new mongosse.Schema(
    {
        user_id: {
            type: mongosse.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        comments: {
            type: String,
            default: 'N/A',
            required: true,
            trim: true,
        },
    }, {timestamps: true},
);


const Attendee = mongosse.model('Attendee', attendeeSchema);
module.exports = Attendee;
