const mongosse = require('mongoose');

//enum for status
const eventSchema = new mongosse.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
        },
        slug: {
            type: String,
            required: true,
            trim: true,
            unique: true,
            index:true,
        },
        status: {
            type: String,
        },
        user_id: {
            type: mongosse.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        location: {
            type: String,
            required: true,
        },
        start_date: {
            type: String,
            required: true,
        },
        comments: {
            type: String,
            required: true,
        },
        price: {
            type: Number,
            required: true,
        },
        attendees: [
            {
                type: mongosse.Schema.Types.ObjectId,
                ref: 'Attendee',
                required: true,
            }
        ]
    }, {timestamps: true},
);

eventSchema.methods.addAttendee = function (offer) {
    this.attendees.push(offer);
    this.save();
};

eventSchema.methods.assignTask = function (user) {

};

const Event = mongosse.model('Event', eventSchema);
module.exports = Event;
