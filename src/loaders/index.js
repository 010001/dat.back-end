const expressLoader = require('./express');
const mongooseLoader = require('./mongoose');

exports.init = (expressApp) => {
    const mongoConnection = mongooseLoader();
    expressLoader(expressApp);

    // ... more loaders can be here

    // ... Initialize agenda
    // ... or Redis, or whatever you want
};
