const mongoose = require('mongoose');

module.exports = async function () {

    const connection = await mongoose.connect(process.env.MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    }).catch(() => {
        console.log("error");
        process.exit(1);
    });

    //TODO:throw new error when can't connect


    return connection.connection.db;
};
