const express = require('express');
// import * as bodyParser from 'body-parser';
const cors = require('cors');
const apiRouter = require('../../src/routes/v1/api');
const config = require('../../src/config/app');
const rateLimit = require("express-rate-limit");
const helmet = require('helmet');
const createGracefulShutdownMiddleware = require('express-graceful-shutdown');
const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100 // limit each IP to 100 requests per windowMs
});

const whitelist = ['http://love.theinvulnerable.com.au', '*'];
const corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
};
module.exports = async (app) => {
    if (config.node_env === 'production') {
        app.use(cors(corsOptions));
    } else {
        app.use(cors());
    }
    app.use(helmet());
    app.use(express.json());
    app.use(limiter);
    app.use(config.api.prefix, apiRouter);
    app.use(createGracefulShutdownMiddleware(app, {forceTimeout: 30000}));
    return app;
};
