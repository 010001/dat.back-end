const faker = require('faker');

exports.taskMod = () => {
    return {
        _id: 1,
        type_id: 1,//Type = cleaning
        comments: faker.lorem.paragraph(),
        time: 2222434,
        created_at: 34324,
        updated_at: 324234,
        status: "open",
        user_id: 1,
        user: this.userMod(),
        assigned_id: 2,
        address: faker.address.city() + faker.address.streetAddress(),
        offers: [
            this.offersMod(),
            this.offersMod(),
        ],
        task_type: "clean",
        //may change computers: 5
        type: this.getValidationType("clean")
    }
};

exports.getValidationType = (task_type) => {
    let v = {
        room: 1,
        bathroom: 1,
        end_of_lease: true,
        price: 100,
    }

    switch (task_type) {
        case 'clean':
            return v;
            break;
    }
}

//validate

exports.userMod = () => {
    return {
        user_name: faker.internet.userName(),
        email: faker.internet.email(),
        name: faker.name.firstName() + ' ' + faker.name.lastName(),
        profile_img: 'https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/3480854/photo-c138420713406181a9422e46f4d83ee9.jpg',
    };
};

exports.offersMod = () => {
    return {
        user_id: 4,
        user: {
            name: 'Tina Z',
            rate: 5,
            people: 35,
            completion_rate: 90,
        },
        price: 100,
        comments: faker.lorem.paragraph(),
        profile_img: ''
    }
};
