const status = require('http-status');
const Attendee = require('../../../../models/attendee');
const Event = require('../../../../models/event');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

exports.store = async (req, res) => {
    const attend = new Attendee(req.body);
    const eventId = req.body.event_id;
    try {
        const off = await attend.save().then(t => t.populate('attendee_id').execPopulate());

        Event.findById(eventId).then(attendee => {
            if (!attendee) {
                throw new Error("Cannot find Event");
            }
            attendee.addAttendee(attend);
            res.status(status.CREATED).send({attend: off});
        });
    } catch (e) {
        res.status(400).send(e);
    }

    //model
    // res.status(status.CREATED).json({});
};

exports.update = async (req, res) => {
    const {id} = req.params;
    await Attendee.findByIdAndUpdate(ObjectId(id), req.body, {new: true});
    res.status(200).json({});
    // const attendee = new Attendee(req.body);
    // try {
    //     await attendee.save();
    //     res.status(status.CREATED).send({attendee});
    // } catch (e) {
    //     res.status(400).send(e);
    // }
    //
    // //model
    // res.status(status.CREATED).json({});
};


exports.delete = async (req, res) => {
    Attendee.findByIdAndDelete(ObjectId(req.params.id)).then(function (err) {
        if (err || err === null) {
            res.status(500).send(err);
        }
    }).catch(err => console.log(err));

    res.status(status.NO_CONTENT).json({});
};
