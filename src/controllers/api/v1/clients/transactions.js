const Transaction = require('../../../../models/transaction');
const status = require('http-status');
exports.store = async(req,res) => {
    const trans = new Transaction(req.body);
    try {
        const result = await trans.save();
        res.status(status.CREATED).send({transaction:result});
    } catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
};

exports.show = async (req,res) => {
    // const transaction = await Transaction.find({_id:req.params.id});
    // res.send(transaction);
};

exports.showMe = async (req,res) => {
    try {
        const transaction = await Transaction.find({});
        res.send(transaction);
    } catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
};
