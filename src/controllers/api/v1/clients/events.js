const status = require('http-status');
const Event = require('../../../../models/event');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const {validationResult} = require('express-validator');

exports.index = async (req, res) => {
    const title = req.params.title;
    let search = {status: 'open'};
    if (title) {
        search = {title};
    }
    const event = await Event.find(search).populate('user_id');
    res.send(event);
};

exports.show = async (req, res) => {



    Event.findOne({slug: req.params.slug}).populate('user_id').populate({
        'path': 'attendees',
        populate: {path: 'user_id'}
    }).then(event => {
        res.status(200).send({event});
    });
};

exports.indexMe = async (req, res) => {
    const userId = req.params.id;
    await Event.find({user_id: userId}, function (err, data) {
        if (err) res.status(500).send("error");
        res.status(200).send(data);
    });
};

exports.update = async (req, res) => {
    const {id} = req.params;
    const event = await Event.findByIdAndUpdate(ObjectId(id), req.body, {new: true});
    res.status(200).send(event);
};


exports.delete = async (req, res) => {
    try {
        Event.findByIdAndRemove(ObjectId(req.params.id), function (err) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(status.NO_CONTENT).json({});
            }
        });
    } catch (e) {
        res.status(500).send(e);
    }
};

exports.store = async (req, res) => {
    //need type_id ???? front-end
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({errors: errors.array()});
    }

    const event = new Event(req.body);
    event.status = "open";
    try {
        await event.save().then(t => t.populate('user_id').execPopulate());
        res.status(status.CREATED).send({event});
    } catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
};


exports.addAttachment = async (req, res) => {
    try {
        const {id} = req.params;
        const event = await Event.findByIdAndUpdate(ObjectId(id), req.body, {new: true});
        event.attachments.push(req.body.attachment);
        await event.save();
        res.status(200).send(event);
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.search = async (req, res) => {
    const title = req.query.title;
    let search = {status: 'open', name: new RegExp(title, 'i')};

    const events = await Event.find(search).populate('user_id');
    res.send(events);
};


