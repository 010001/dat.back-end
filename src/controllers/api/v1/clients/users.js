const {sendEmail} = require("../../../../services/email");
const User = require('../../../../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Review = require('../../../../models/review');
exports.index = async (req, res) => {
    try {
        const users = await User.find({});
        res.send(users);
    } catch (e) {
        res.status(500).send();
    }
};

exports.update = async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password'];
    const isValid = updates.every((update) => {
        return allowedUpdates.includes(update);
    });

    if (!isValid) {
        return res.status(400).send({error: 'Invalid Updates!'});
    }

    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true});
        if (!user) {
            return res.status(404).send();
        }
        res.send(user);
    } catch (e) {
        res.status(404).send(e);
    }
};

exports.destroy = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.param.id);
        if (!user) {
            return res.status(404).send();
        }
        res.send(user);
    } catch (e) {
        res.status(500).send();
    }
};

exports.store = async (req, res) => {
    //typeID
    const user = new User(req.body);

    try {
        await user.save();
        const token = await user.generateAuthToken();
        res.status(201).send({user, token});
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.show = async (req, res) => {
    try {
        const user =await User.find({_id: req.query.id});
        res.send(user);
    } catch (e) {
        console.log(e);
        res.status(500).send();
    }
};

exports.updateUserMe = async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password'];
    const isValid = updates.every((update) => {
        return allowedUpdates.includes(update);
    });

    if (!isValid) {
        return res.status(400).send({error: 'Invalid Updates!'});
    }

    try {
        updates.forEach((update) => req.user[update] = req.body[update]);
        await req.user.save();
        res.send(user);
    } catch (e) {
        res.status(404).send(e);
    }
};
exports.deleteUserMe = async (req, res) => {
    try {
        await User.remove({email:req.body.email});
        res.send(req.user);
    } catch (e) {
        console.log(e);
        res.status(500).send();
    }
};

exports.logoutAll = async (req, res) => {
    try {
        req.user.tokens = [];
        await req.user.save();
        res.send();
    } catch (e) {
        res.status(500).send();
    }
};

exports.logout = async (req, res) => {
    try {
        req.user.tokens = req.user.token.filter((token) => {
            return token.token !== req.token;
        });
        await req.user.save();
        res.send();
    } catch (e) {
        res.status(500).send();
    }
};

exports.signIn = async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({user, token});
    } catch (e) {
        res.status(e.toString().split(": ")[1]).send(e.toString());
    }
};
//
// exports.googleSignIn = async (req, res) => {
//     try {
//         const user = await User.findOne({'Google.GoogleID': req.body.Google.GoogleID, 'Google.email':req.body.Google.email});
//          const token = await user.generateAuthToken();
//         res.send({user, token});
//     } catch(e) {
//         console.log(e);
//         res.status(404).send(e.toString());
//     }
// }
//
// exports.facebookSignIn = async (req, res) => {
//     try {
//         const user = await User.findOne({'FaceBook.FaceBookID': req.body.FaceBook.FaceBookID,
//         'FaceBook.email':req.body.FaceBook.email});
//         const token = await user.generateAuthToken();
//         res.send({user, token});
//     } catch(e) {
//         console.log(e);
//         res.status(404).send(e.toString());
//     }
// }


exports.signUp = async(req, res) => {
    try{
        const postData = {
            email: req.body.email,
            password: req.body.password
        };

        User.findOne({email: postData.email}, function(err, data){
            if (data){
                res.status(302).send('the email is already taken.');
            } else {
                sendEmail([req.body.email], function () {
                    // console.log("abc");
                });
                User.create(postData, function(err, data) {
                    if (err) {
                        console.log(err);
                        res.status(500).send(err.message.message);
                    }else {
                        res.status(201).send({user:data});
                    }
                })
            }
        })
    } catch(e) {
        res.status(400).send(e);
    }
}

exports.getUserMe = async (req, res) => {
    const _id = req.params.id;
    console.log(_id);
    try {
        const user = User.find(_id);
        if (!user) {
            return res.status(404).send()
        }
        res.send(user);
    } catch (e) {
        res.status(500).send();
    }
};
exports.sendForgetEmail = async (req, res) => {
    const email = req.params.email;
    const user = await User.findOne({email: email}, function(err,data){
        if (err) {res.status(500).send('server has exceptions')}
        else {
            res.status(200).send();
        }
    }).exec();

    sendEmail([req.params.email], function () {
        // console.log("abc");
    });

}
exports.getUserDefault = async (req, res) => {
    const email = req.params.email;
    const user = await User.findOne({email: email}, function(err,data){
        if (err) {console.log(err);res.status(500).send('server has exceptions');}
        if (data == null){
            res.status(400).send("error");
        } else {
            res.status(200).send(data);
        }
    }).exec();
}
exports.updateUserInformation = async (req, res) => {
    const email = req.body.email;
    await User.findOne({email}, function(err,data){
            if (err)  {console.log(err);res.status(500).send('server has exceptions');}
            const user = User.findByIdAndUpdate({_id: data._id}, {$set:req.body}, function(err, data){
            if (err)  {console.log(err);res.status(500).send('server has exceptions');}
        }).exec();
    })
    const user = await User.findOne({email:email}, function(err,data){
        if (err) {console.log(err);res.status(500).send('server has exceptions');}
    })
    const token  =  await user.generateAuthToken();
    res.send({user, token});
}

exports.getPosterRating = async (req, res) => {
        try {
            const review = await Review.find({poster_id: req.params.id})
            res.status(200).send({review});
        } catch (err) {
            console.log(err);
            res.status(400).send(err);
        }
}
exports.getTaskerRating = async (req, res) => {
    try {
        const review = await Review.find({tasker_id: req.params.id})
        res.status(200).send({ review});
    } catch (err) {
        console.log(err);
        res.status(400).send(err);
    }
}

exports.findReviewID = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.body.other_id});
        res.send({user});
    } catch(err) {
        res.status(400).send("No such user");
    }
}
