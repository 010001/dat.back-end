// const stripe = require('../config/stripe');
// const Transaction = require('../models/transaction');
// const postStripeCharge = (res, req) => (stripeErr, stripeRes) => {
//     if (stripeErr) {
//         res.status(500).send({error: stripeErr});
//     } else {
//         const transaction = new Transaction();
//         transaction.poster_id = req.body.poster_id;
//         transaction.eventer_id = req.body.eventer_id;
//         transaction.currency = req.body.currency;
//         transaction.amount = req.body.amount;
//         transaction.description = req.body.description;
//         transaction.save();
//         res.status(200).send({success: stripeRes});
//     }
// };
//
// exports.createCharges = async (req, res) => {
//     return await stripe.charges.create(req.body, postStripeCharge(res, req));
// };

const stripe = require('../config/stripe');
const Transaction = require('../models/transaction');

const postStripeCharge = (res, req) => async (stripeErr, stripeRes) => {

    try {
        if (stripeErr) {
            res.status(500).send({error: stripeErr});
        } else {
            const dataTrans = {
                'amount': req.body.amount,
                'currency': req.body.currency,
                'description': req.body.description,
                'payer_id': req.body.payer_id,
                'payee_id': req.body.payee_id
            };
            const trans = new Transaction(dataTrans);
            await trans.save();
            if (!res) {
                res.status(500).send({error: "cannot save error"});
            }

            res.status(200).send({success: stripeRes});
        }
    } catch (e) {
        res.status(400).send(e);
    }
};

exports.createCharges = async (req, res) => {
    const data = {
        'amount': parseInt(parseInt(req.body.amount) + "00"),
        'currency': req.body.currency,
        'description': req.body.description,
        'source': req.body.source,
    };
    return await stripe.charges.create(data, postStripeCharge(res, req));
};
