const configureStripe = require('stripe');

const STRIPE_SECRET_KEY = process.env.NODE_ENV === 'production'
    ? process.env.STRIPE_SECRET_KEY
    : 'sk_test_l5Qh2UK3kLKcIYvSIwEvbKIV00KKP7LwZB';

const stripe = configureStripe(STRIPE_SECRET_KEY);

module.exports = stripe;
