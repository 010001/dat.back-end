const jwt = require('jsonwebtoken');
const User = require('../models/user');

const isAuth = async (req, res, next) => {

    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const decoded = jwt.verify(token, 'thisismynewcourse');
        const user = await User.findOne({_id: decoded._id, 'tokens.token': token});
        if (!user) {
            res.status(500).send({error: "No this user"});
        }
        req.user = user;
        next();
    } catch (e) {
        console.log(e);
        res.status(401).send({error: "Please authenticate"});
    }

};

module.exports = isAuth;
