const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const path = require('path');
const s3 = new aws.S3({
    region: 'ap-southeast-2',
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY_ID
});

const storage = multerS3({
    s3: s3,
    bucket: 'kitmanimage',
    metadata: function (req, file, cb) {
        cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
        cb(null, Date.now().toString() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
module.exports = upload;
