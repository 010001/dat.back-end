const {check} = require('express-validator');

const post = [
    // username must be an email
    //TODO: Validation for price
    check('name', 'Name is Empty').notEmpty(),
    check('status', 'Status is Empty').notEmpty(),
    check('user_id', 'User ID is Empty').notEmpty(),
    check('location', 'Location is Empty').notEmpty(),
    check('start_date', 'Time is Empty').notEmpty(),
    check('comments', 'Comments is Empty').notEmpty(),
    check('price', 'Price is Empty').notEmpty().isNumeric(),
    check('slug', 'Slug is Empty').notEmpty(),
    check('type_id', 'TypeID is Empty').notEmpty()
];

const get = [
    // username must be an email
    check('title', 'Title is not string').isString(),
];

const show = [
    // username must be an email
    check('title', 'Title is not string').isString(),
];

const update = [
    // username must be an email
    check('title', 'Title is not string').isString(),
];

const del = () => [
    // username must be an email
    check('title', 'Title is not string').isString(),
];


module.exports = {
    post,
    get,
    show,
    update,
    del,
};
