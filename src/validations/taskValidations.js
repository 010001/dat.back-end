const { check } = require('express-validator');

const postUserValidationRules = () => {
    return [
        // username must be an email
        check('name','Name is Empty').notEmpty(),
        check('status','Status is Empty').notEmpty(),
        check('user_id','User ID is Empty').notEmpty(),
        check('address','Address is Empty').notEmpty(),
        check('time','Time is Empty').notEmpty(),
        check('comments','Comments is Empty').notEmpty(),
        check('offerPrice','Offer Price is Empty').notEmpty(),
        check('slug','Slug is Empty').notEmpty(),
        check('type_id','TypeID is Empty').notEmpty()
    ]
};

const getUserValidationRules = () => {
    return [
        // username must be an email
        check('title','Title is not string').isString(),
    ]
};

module.exports = {
    postUserValidationRules,
    getUserValidationRules,
};
