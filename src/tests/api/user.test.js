const request = require('supertest');
const app = require('../../../app');
const User = require('../../models/user');

// beforeEach(async ()=>{
// const user = await User.findOne({email:"abc@gmail.com"});
// console.log(user._id);
// })

// //mine
test("Get Users", async () => {
        await request(app).get('/api/v1/users').expect(200);
})

test("Create new User", async () => {
        await request(app).post('/api/v1/users/')
        .send({email:"abc@gmail.com", password:"12345678910"})
        .set('Accept', 'application/json').expect(201)
})

test("Get Users me", async () => {
        await request(app).get('/api/v1/users/me').expect(200);
})

test("Update Users not found", async () => {
        await request(app).patch('/api/v1/users/61651').expect(404);
})

test("Update Users success", async () => {
        const user = await User.findOne({email:"abc@gmail.com"});
        await request(app).patch(`/api/v1/users/${user._id}`)
        .expect(200);
})

// delete('/users/:id) does not work
//get('users/me') does not work
//delete('users/me') does not work
//patch('users/me') does not work
//post('/users/logoutAll') does not work
//post('/users/logout') does not work


test("Forget email", async () => {
        await request(app).post('/api/v1/users/forgetabc@gmail.com')
        .expect(200);
})

test("Update Users By Email authorized", async () => {
        const user = await User.findOne({email:"abc@gmail.com"});
        await request(app).put('/api/v1/users/updateOne')
        .send({_id:user._id,email:"abc@gmail.com", token:user.tokens[0].token})
        .expect(200);
})

test("Delete Users but Unauthorized", async () => {
        await request(app).delete(`/api/v1/users/12345`).expect(401);
})

test("Delete Users success", async () => {
        const user = await User.findOne({email:"abc@gmail.com"});
        await request(app).delete(`/api/v1/users`)
        .send({email:"abc@gmail.com"})
        .expect(200);
})

//mine

test("Get Users By Email", async () => {
        await request(app).get('/api/v1/users/me/getxiaoishandsome@gmail.com').expect(200);
})

test("Get Users All", async () => {
        await request(app).get('/api/v1/users').expect(200);
})
test("Login in user", async () => {
        await request(app).post('/api/v1/users/signIn')
        .send({email:"kitmanwork@gmail.com", password:"12345678910"})
        .set('Accept', 'application/json').expect(200)
})

test("Login in user failed by wrong password", async () => {
        await request(app).post('/api/v1/users/signIn')
        .send({email:"kitmanwork@gmail.com", password:"1234567891011"})
        .set('Accept', 'application/json').expect(401)
})

test("Login in user failed  by wrong email", async () => {
        await request(app).post('/api/v1/users/signIn')
        .send({email:"kitmanworkddd@gmail.com", password:"12345678910"})
        .set('Accept', 'application/json').expect(401)
})

test("Delete user", async () => {
        await request(app).delete('/api/v1/users')
        .send({email:"kitmanwork12345@gmail.com"})
        .set('Accept', 'application/json').expect(200)
})

test("Sign up user failed by password less than 7", async () => {
        await request(app).post('/api/v1/users/signUp')
        .send({email:"kitmanwork12345@gmail.com", password:"123456"}).expect(500)
})

test("Sign up user failed by wrong email format", async () => {
        await request(app).post('/api/v1/users/signUp')
        .send({email:"kitmanwork12345gmail.com", password:"123456"}).expect(500)
})

test("Sign up user success", async () => {
        await request(app).post('/api/v1/users/signUp')
        .send({email:"kitmanwork12345@gmail.com", password:"12345678910"})
        .set('Accept', 'application/json').expect(201)
})

test("Sign up user repeatedly", async () => {
        await request(app).post('/api/v1/users/signUp')
        .send({email:"kitmanwork12345@gmail.com", password:"12345678910"})
        .set('Accept', 'application/json').expect(302)
})

test("Delete user", async () => {
        await request(app).delete('/api/v1/users')
        .send({email:"kitmanwork12345@gmail.com"})
        .set('Accept', 'application/json').expect(200)
})

