const request = require('supertest');
const app = require('../../../app');
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000
const Event = require('../../models/event');

beforeEach(async ()=>{

})
afterEach(async ()=>{

})
const mongoose = require('mongoose');
const user_id = "5e047d46316d3a09bb4faac8"
const event_one = {
    name:'j',
    slug:'Cleaning pool',
    type_id:1,
    status:'open',
    user_id:user_id,
    address:'h',
    time:'2020-01-01',
    comments:'Asking someone to clean pool',
    offerPrice:'200'
}
const event_two = {
    name:'j',
    slug:'Cleaning pool',
    type_id:1,
    status:'close',
    user_id:user_id,
    address:'h1',
    time:'2020-01-02',
    comments:'Asking someone to clean pool',
    offerPrice:'200'
}


test('View 1 Events', async () =>{
    await request(app).get('/api/v1/events/1').expect(200);
});


//創建Event
test('Post Events fail due to empty', async () =>{
    await request(app).post('/api/v1/events')
    .send({})
    .expect(422);
});

test('Post Events success', async () =>{
    await request(app).post('/api/v1/events')
    .send(event_one)
    .expect(201);
});

//取得Events index
test('Get Events', async () =>{
    await request(app).get('/api/v1/events').expect(200);
});

//取得event index_me
// test('get me id', async () =>{
//     await request(app).get('/api/v1/events/me/5e046c436ad535078bdb0159')
//     .expect(200);
// });

// 預設params 會從URL上取得, 透過id 取得event
test('update id', async () =>{
    const event = await Event.find({user_id:user_id})
    const res = await request(app).put(`/api/v1/events/${event[0]._id}`)
    .send(event_two)
    .expect(200);
    // const user = await Event.findById(res.body._id)

});


// 預設params 會從URL上取得, 透過slug取得event
test('show by Slug', async () =>{
    await request(app).get('/api/v1/events/clean')
    .expect(200);
});

test('add attachment', async () =>{
    const event = await Event.find({user_id:user_id})
    await request(app).post(`/api/v1/events/addAttachment/${event[0]._id}`)
    .expect(200);
});

// cannot set headers after they are sent to the client
test('delete failed', async () =>{
    await request(app).delete('/api/v1/events/12345')
    .expect(500);
});

test('delete by event id', async () =>{
    //用預設的user_id找到剛創建的event
    const event = await Event.find({user_id:user_id})
    await request(app).delete(`/api/v1/events/${event[0]._id}`)
    .expect(204);
});





